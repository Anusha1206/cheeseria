"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var routes_1 = require("./routes");
var app = express();
const cors = require('cors');
app.use(cors());
app.use(express.static('public'));
app.use(routes_1.default);
var port = process.env.PORT || 3000;
app.listen(port, function () { return console.log("Server listening on port: " + port); });
//# sourceMappingURL=server.js.map