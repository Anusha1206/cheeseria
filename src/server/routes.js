"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var cheeses = require('./data/cheeses.json');
var cartItems = require('./data/cartItems.js');
var router = express.Router();
var cors = require('cors');
router.use(cors());
router.get('/api/cheeses', function (req, res, next) {
    res.json(cheeses);
});
/*router.get('/api/products', (req, res, next) => { //lists all  available products
    res.json(cartItems);
});
*/
router.post('/api/products', function (req, res) {
    var id = null;
    var products;
    var cart = JSON.parse(req.body.cartitems);
    if (!cart)
        return res.json(products);
    for (var i = 0; i < cartItems.products.length; i++) {
        id = cartItems.products[i].id.toString();
        products.push(cartItems.products[i]);
    }
    return res.json(products);
});
exports.default = router;
//# sourceMappingURL=routes.js.map