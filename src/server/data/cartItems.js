const products = [
    {
        "id": 6,
        "title": "MAASDAM",
        "price": 140,
        "description": "Maasdam is a traditional, semi-hard Dutch cheese made from cow�s milk. The most characteristic feature is its �eyes� (holes) that make up most of the cheese. The cheese was created in the early 1990s as an alternative to more expensive Swiss Emmental cheese. It is a high-fat cheese with a minimum of 45% fat. Although similar to Emmental, the moisture content in Maasdam is more, making it suppler.",
        "category": "creamy, open and supple",
        "image": "https://www.cheese.com/media/img/cheese/wiki/maasdam.jpg"
    },
        
]

module.exports = { 'products': products }