import * as express from 'express';

const cheeses = require('./data/cheeses.json');

const cartItems = require('./data/cartItems.js');

const router = express.Router();
const cors = require('cors');
router.use(cors());

type CartItemType = {
    id: number;
    category: string;
    description: string;
    image: string;
    price: number;
    title: string;
    amount: number;
};
router.get('/api/cheeses', (req, res, next) => {
    res.json(cheeses);
});

/*router.get('/api/products', (req, res, next) => { //lists all  available products
    res.json(cartItems);
});
*/

router.post('/api/products', (req, res) => { //generates the list of products in the cart
    let id = null;
    let products: CartItemType[];
    let cart = JSON.parse(req.body.cartitems);
    if (!cart) return res.json(products)
    for (var i = 0; i < cartItems.products.length; i++) {
        id = cartItems.products[i].id.toString();
        products.push(cartItems.products[i]);
    }
    return res.json(products);
});



export default router;