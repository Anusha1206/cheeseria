"use strict";
var __makeTemplateObject = (this && this.__makeTemplateObject) || function (cooked, raw) {
    if (Object.defineProperty) { Object.defineProperty(cooked, "raw", { value: raw }); } else { cooked.raw = raw; }
    return cooked;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.HeaderTypography = exports.StyledAppBar = exports.StyledButton = exports.Wrapper = void 0;
var styled_components_1 = require("styled-components");
var IconButton_1 = require("@material-ui/core/IconButton");
var core_1 = require("@material-ui/core");
exports.Wrapper = styled_components_1.default.div(templateObject_1 || (templateObject_1 = __makeTemplateObject(["\n  margin: 40px;\n"], ["\n  margin: 40px;\n"])));
// export const StyledButton = styled(IconButton)`
// `;
exports.StyledButton = core_1.withStyles({
    label: {
        flexDirection: "column"
    }
})(IconButton_1.default);
exports.StyledAppBar = styled_components_1.default(core_1.AppBar)(templateObject_2 || (templateObject_2 = __makeTemplateObject(["\n  background : white;\n  margin-bottom: 15px;\n  border-radius: 20px;\n"], ["\n  background : white;\n  margin-bottom: 15px;\n  border-radius: 20px;\n"])));
exports.HeaderTypography = core_1.withStyles({
    root: {
        color: "black",
        WebkitTextStroke: "0.5px darkgoldenrod",
        fontStyle: "italic"
    }
})(core_1.Typography);
var templateObject_1, templateObject_2;
//# sourceMappingURL=App.styles.js.map