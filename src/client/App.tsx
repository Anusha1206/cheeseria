import React, { useState } from 'react';
import { useQuery } from 'react-query';

// Components
import Item from './Cart/Item/Item';
import Cart from './Cart/Cart';
import Drawer from '@material-ui/core/Drawer';
import LinearProgress from '@material-ui/core/LinearProgress';
import Grid from '@material-ui/core/Grid';
import AddShoppingCartIcon from '@material-ui/icons/AddShoppingCart';
import RestoreIcon from '@material-ui/icons/Restore';
import Badge from '@material-ui/core/Badge';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
const BASE_URL = 'http://localhost:3000';

const instance = axios.create();

// Styles
import { Wrapper, StyledButton, StyledAppBar, HeaderTypography } from './App.styles';
import { AppBar, Toolbar, Typography } from '@material-ui/core';
import axios from 'axios';

// Decalring item variables 
var itemTitle: string;
var itemDescription: string;
var itemCategory: string;
var itemPrice: number;
var itemImage: string;

export type CartItemType = {
  id: number;
  category: string;
  description: string;
  image: string;
  price: number;
  title: string;
  amount: number;
};


const getCheeses = async (): Promise<CartItemType[]> =>
    await (await fetch(`api/cheeses`)).json();

// Adding Items from cart to server
export const getPurchasedProducts = (cartitems: CartItemType[]) => {
    return axios.post(`${BASE_URL}/api/purchaseditems`, {cartitems})
      .then(response => response.data);
}

const App = () => {
    const [cartOpen, setCartOpen] = useState(false);
    const [cartItems, setCartItems] = useState([] as CartItemType[]);
    const [dialogOpen, setDialogOpen] = useState(false);
    const { data, isLoading, error } = useQuery<CartItemType[]>(
        'cheeses',
        getCheeses
    );
    console.log(data);

    const getTotalItems = (items: CartItemType[]) =>
        items.reduce((ack: number, item) => ack + item.amount, 0);

    const getClickedItemDetails = (clickedItem: CartItemType) => {
        itemTitle = clickedItem.title;
        itemDescription = clickedItem.description;
        itemCategory = clickedItem.category;
        itemPrice = clickedItem.price;
        itemImage = clickedItem.image;
        handleDialogOpen();      
    };
  
    const handleDialogOpen = () => {       
        setDialogOpen(true);
    };

    const handleDialogClose = () => {
        setDialogOpen(false);
    };

  const handleAddToCart = (clickedItem: CartItemType) => {
    setCartItems(prev => {
      // 1. Is the item already added in the cart?
      const isItemInCart = prev.find(item => item.id === clickedItem.id);

      if (isItemInCart) {
        return prev.map(item =>
          item.id === clickedItem.id
            ? { ...item, amount: item.amount + 1 }
            : item
        );
      }
      // First time the item is added
      return [...prev, { ...clickedItem, amount: 1 }];
    });
  };

  const handleRemoveFromCart = (id: number) => {
    setCartItems(prev =>
      prev.reduce((ack, item) => {
        if (item.id === id) {
          if (item.amount === 1) return ack;
          return [...ack, { ...item, amount: item.amount - 1 }];
        } else {
          return [...ack, item];
        }
      }, [] as CartItemType[])
    );
  };

  if (isLoading) return <LinearProgress />;
  if (error) return <div>Something went wrong ...</div>;

  return (

    <Wrapper>
      <StyledAppBar position="static">
        <Toolbar>
          <Grid
            container
            direction="row"
            justify="space-between"
            alignItems="center"
          >
            <StyledButton>
              <RestoreIcon />
              <Typography variant="subtitle2">
                Recent Purchases
              </Typography>
            </StyledButton>

            <HeaderTypography variant="h3" noWrap>
              Welcome to Patient Zero's Cheeseria
            </HeaderTypography>

            <StyledButton onClick={() => setCartOpen(true)}>
              <Badge
                badgeContent={getTotalItems(cartItems)}
                color='error'
                data-cy="badge-count">
                <AddShoppingCartIcon />
              </Badge>

              <Typography variant="subtitle2">
                Cart
              </Typography>
            </StyledButton>
                      <Dialog
                          open={dialogOpen}
                          onClose={handleDialogClose}
                          aria-labelledby="alert-dialog-title"
                          aria-describedby="alert-dialog-description"
                      >
                          <DialogTitle id="alert-dialog-title">{"Item Details"}</DialogTitle>
                          <DialogContent>
                              <DialogContentText id="alert-dialog-description">
                                  <b> {itemTitle}, </b> <b> {itemCategory} </b> <b> ${itemPrice} </b> <br /> <br />
                                  <img src={itemImage} alt={itemTitle} width="550" height="350" /> <br /> <br />
                                  {itemDescription} <br />                                 
                              </DialogContentText>                            
                           </DialogContent>
                          <DialogActions>
                              <Button onClick={handleDialogClose} color="primary">
                                  Cancel
                              </Button>
                              <Button onClick={handleDialogClose} color="primary" autoFocus>
                                 Okay
                              </Button>
                          </DialogActions>
                      </Dialog>
          </Grid>
        </Toolbar>
      </StyledAppBar>

      <Drawer anchor='right' open={cartOpen} onClose={() => setCartOpen(false)}>
        <Cart
          cartItems={cartItems}
          addToCart={handleAddToCart}
          removeFromCart={handleRemoveFromCart}
        />
      </Drawer>

      <Grid container spacing={3}>
        {data?.map(item => (
          <Grid item key={item.id} xs={12} sm={4}>
                <Item item={item} handleAddToCart={handleAddToCart} getClickedItemDetails={getClickedItemDetails} />
          </Grid>
        ))}
      </Grid>
    </Wrapper>

  );
};

export default App;
