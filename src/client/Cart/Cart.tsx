import CartItem from './CartItem/CartItem';
import { Wrapper } from './Cart.styles';
import { CartItemType, getPurchasedProducts} from '../App';
import Button from '@material-ui/core/Button';

type Props = {
  cartItems: CartItemType[];
  addToCart: (clickedItem: CartItemType) => void;
  removeFromCart: (id: number) => void;

};

var cartitems: CartItemType[];
cartitems = [
    {
        "id": 10,
        "title": "ABBAYE DE BELLOC",
        "price": 109.95,
        "description": "Abbaye de Belloc is a flat wheel-shaped traditional, farmhouse, unpasteurised, semi-hard cheese made from sheep's milk. It has a natural, crusty, brownish rind with patches of red, orange and yellow. The rind is marked with tiny craters.",
        "category": "creamy, dense and firm",
        "image": "https://www.cheese.com/media/img/cheese/Abbaye-de-Belloc.jpg",
        "amount": 70
    }

]

const Cart: React.FC<Props> = ({ cartItems, addToCart, removeFromCart}) => {
  const calculateTotal = (items: CartItemType[]) =>
    items.reduce((ack: number, item) => ack + item.amount * item.price, 0);

  return (
    <Wrapper>
      <h2>Your Shopping Cart</h2>
      {cartItems.length === 0 ? <p>No items in cart.</p> : null}
      {cartItems.map(item => (
        <CartItem
          key={item.id}
          item={item}
          addToCart={addToCart}
          removeFromCart={removeFromCart}
        />
      ))}
          <h2>Total: ${calculateTotal(cartItems).toFixed(2)}</h2>

          <Button className="btn btn-primary float-right" onClick={() => getPurchasedProducts(cartItems)}
              style={{ marginRight: "10px" }}> Purchase </Button>
    </Wrapper>
  );
};

export default Cart;
